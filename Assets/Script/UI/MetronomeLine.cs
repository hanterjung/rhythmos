﻿using UnityEngine;
using System.Collections;

public class MetronomeLine : MonoBehaviour {

	private Vector3 _startPoint, _endPoint;
	private Vector3 startPoint, endPoint;
	private LineRenderer[] lines;


	void Awake () {
		lines = new LineRenderer[4];
		lines[0] = CreateAndGetLine ("LeftLine");
		lines[1] = CreateAndGetLine ("TopLine");
		lines[2] = CreateAndGetLine ("RightLine");
		lines[3] = CreateAndGetLine ("BottomLine");

		_startPoint = new Vector3 (0, 0, -0.01f);
		_endPoint = new Vector3 (100, 100, -0.01f);
	}

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		/*Vector2 p1 = Vector2.zero;
		Vector2 p2 = Vector2.zero;

		p1.x = 0;
		p1.y = 0;
		p2.x = Screen.width;
		p2.y = 0;
		p1 = parentCamera.WorldToScreenPoint (p1);
		p2 = parentCamera.WorldToScreenPoint (p2);

		Texture2D lineTex = new Texture2D (1, 1);

		Color color = Color.white;
		GUI.color = color;

		float length = (p1 - p2).magnitude;

		GUI.DrawTexture (new Rect (p1.x, p2.x, length, 1), lineTex);*/

		//ShowMetronomeLine ();
	}

	void ShowMetronomeLine() {
		startPoint = _startPoint;
		endPoint = _endPoint;

		startPoint = Camera.main.WorldToScreenPoint (_startPoint);
		endPoint = Camera.main.WorldToScreenPoint (_endPoint);

		DrawRectangle ();
	}

	private void DrawRectangle() {
		lines[0].SetPosition(0, new Vector3(startPoint.x, endPoint.y, 0));
		lines[0].SetPosition(1, new Vector3(startPoint.x, startPoint.y, 0));

		lines[1].SetPosition(0, new Vector3(startPoint.x, startPoint.y, 0));
		lines[1].SetPosition(1, new Vector3(endPoint.x, startPoint.y, 0));

		lines[2].SetPosition(0, new Vector3(endPoint.x, endPoint.y, 0));
		lines[2].SetPosition(1, new Vector3(endPoint.x, startPoint.y, 0));

		lines[3].SetPosition(0, new Vector3(startPoint.x, endPoint.y, 0));
		lines[3].SetPosition(1, new Vector3(endPoint.x, endPoint.y, 0));
	}

	private LineRenderer CreateAndGetLine(string lineName) {
		GameObject lineObject = new GameObject (lineName);
		LineRenderer line = lineObject.AddComponent<LineRenderer> ();
		line.SetWidth (0.05f, 0.05f);
		line.SetVertexCount (2);
		return line;
	}
}
