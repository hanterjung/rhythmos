﻿using UnityEngine;
using System.Collections;

public class BattleGameController : MonoBehaviour {
	//const, static variable
	//private const int DRUM_IDX_KICK = 0;
	//private const int DRUM_IDX_SNARE = 1;
	private readonly float screenWidth, screenHeight;
	private readonly float screenHalfWidth, screenHalfHeight;

	//camerascript for draw metronome line
	public BattleCameraScript cameraScript;
	public Camera gameCamera;

	//audio clips for drum sound
	public AudioClip audioClipBaseDrum;
	public AudioClip audioClipKickDrum;
	public AudioClip audioClipSnareDrum;

	public AudioClip audioClipSongForward;
	public AudioClip audioClipSongAttack;

	//drum ui
	private Vector3 effectSize;
	private Vector2 effectScreenSize;
	public GameObject effectKickDrum;
	public GameObject effectSnareDrum;

	//use to check for beat corretly
	private int cntFixedUpdate = 0;

	//to judge input drum - each 10 fps
	public const int JUDGE_TIME_PERFECT = 1;   // 9~11
	public const int JUDGE_TIME_EXCELLENT = 2; // 8~12
	public const int JUDGE_TIME_GOOD = 3;		 // 7~13
	public const int JUDGE_TIME_BAD = 5;		 // 5~14

	private int judgeState = Beats.JUDGE_BAD;
	private bool isPlaySong = false;

	//to check input drum beat order
	private const int BEAT_TIME_MAX = 8;
	private bool bAllowInputBeat = false;
	private int beatTime = 0;
	private Beats[] beats;
	private Beats lastBeats; //for next song

	//Soldiers
	public Soldier centerSoldier;
	public Soldier[] soldiers;

	//enemy
	public Soldier[] enemySoldiers;

	private static BattleGameController _gameController = null;
	public BattleGameController() {
		screenWidth = Screen.width;
		screenHeight = Screen.height;
		screenHalfWidth = screenWidth / 2;
		screenHalfHeight = screenHalfHeight / 2;

		_gameController = this;
	}

	public static BattleGameController GameController {
		get { return _gameController; }
	}

	void Awake () {
		beats = new Beats[8];
		for(int i=0; i<8; i++) {
			beats[i] = new Beats();
		}
		lastBeats = new Beats(); //for next song
	}

	// Use this for initialization
	void Start () {
		SpriteRenderer sr = effectKickDrum.GetComponent<SpriteRenderer>() as SpriteRenderer;
		effectSize = sr.bounds.size;
		effectScreenSize = gameCamera.WorldToScreenPoint (effectSize);

		cntFixedUpdate = 0;
		judgeState = Beats.JUDGE_BAD;
		isPlaySong = false;
		bAllowInputBeat = false;
		beatTime = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (Application.platform == RuntimePlatform.Android ||
				Application.platform == RuntimePlatform.IPhonePlayer) {

			int touchNum = Input.touchCount;
			Touch touch;
			for (int touchCnt=0; touchCnt<touchNum; touchCnt++) {
				touch = Input.GetTouch(touchCnt);
				if(touch.phase == TouchPhase.Began) {
					Vector2 touchPos = Input.GetTouch(touchCnt).position;

					if(touchPos.x < screenHalfWidth) {
						//left touch
						HitDrum(Beats.BEAT_KICK);
					} else {
						//right touch
						HitDrum(Beats.BEAT_SNARE);
					}
				}
			}

		} else {
			if (Input.GetMouseButtonDown(0)) {
				Vector2 touchPos = Input.mousePosition;
				//Debug.Log(touchPos.x + "," + touchPos.y);
				if(touchPos.x < screenHalfWidth) {
					//left touch
					HitDrum(Beats.BEAT_KICK);
				} else {
					//right touch
					HitDrum(Beats.BEAT_SNARE);
				}
			} else if (Input.GetKeyDown(KeyCode.LeftArrow)) {
				HitDrum(Beats.BEAT_KICK);
			} else if (Input.GetKeyDown(KeyCode.RightArrow)) {
				HitDrum(Beats.BEAT_SNARE);
			}
		}
	}

	private int cntFixedUpdateBeat = 0;
	private int judgeBeatCnt;

	void FixedUpdate() {
		if (cntFixedUpdate == 0) {
			int songType = Beats.GetSongType(beats);
			BattleCameraScript.SetDebugString("SongType : " + Beats.GetSongTypeString(songType));

			if(songType == Beats.SONG_TYPE_NONE) {
				isPlaySong = false;
			} else {
				isPlaySong = true;
				PlaySong(songType);
			}
			
			Beats.Init(beats);

			//order to solider ( act )
			SoldierAction(songType);
		}

		if (cntFixedUpdate > 150 + JUDGE_TIME_BAD) {
			bAllowInputBeat = false;
		} else if (cntFixedUpdate == 80 - JUDGE_TIME_BAD) {
			cntFixedUpdateBeat = 0;
			bAllowInputBeat = true;
		}

		//judge beat input
		if (cntFixedUpdate >= 80 - JUDGE_TIME_BAD && cntFixedUpdate <= 150 + JUDGE_TIME_BAD) {
			if(cntFixedUpdateBeat % 10 == 0) beatTime = cntFixedUpdateBeat/10;

			//judgeBeatCnt = ((cntFixedUpdateBeat + JUDGE_TIME_GOOD) % 10) - JUDGE_TIME_GOOD;		//restore origin cntFixedUpdate -> remain's counter
			//judgeBeatCnt = ((cntFixedUpdateBeat+JUDGE_TIME_BAD) - (beatTime*10)) % 10;
			judgeBeatCnt = (cntFixedUpdateBeat+JUDGE_TIME_BAD) - ((beatTime+1)*10);
			//Debug.Log("beatTime["+beatTime+"]:"+judgeBeatCnt);

			//judgeBeatCnt is -JUDGE_TIME_GOOD ~ +JUDGE_TIME_GOOD+a
			if(judgeBeatCnt <= JUDGE_TIME_PERFECT && judgeBeatCnt >= -JUDGE_TIME_PERFECT) {
				judgeState = Beats.JUDGE_PERFECT;
			} else if(judgeBeatCnt <= JUDGE_TIME_EXCELLENT && judgeBeatCnt >= -JUDGE_TIME_EXCELLENT) {
				judgeState = Beats.JUDGE_EXCELLENT;
			} else if(judgeBeatCnt <= JUDGE_TIME_GOOD && judgeBeatCnt >= -JUDGE_TIME_GOOD) {
				judgeState = Beats.JUDGE_GOOD;
			} else {
				judgeState = Beats.JUDGE_BAD;
			}

			/*Debug.Log("cntFixedUpdate:" + cntFixedUpdate +
			          ", cntFixedUpdateBeat:" + cntFixedUpdateBeat + ", beatTime:" + beatTime +
			          ", calcValue:" + ((cntFixedUpdateBeat+JUDGE_TIME_BAD) - (beatTime*10)) +
			          ", judgeBeatCnt:" + judgeBeatCnt + ", judgeState:" + judgeState);*/

			//judgeBeatCnt+=1;	//only using ouput debug string
			cntFixedUpdateBeat++;
		}

		//fixed update's frequency = 0.025 (40fps)
		if (cntFixedUpdate >= 80) {
			if (cntFixedUpdate % 10 == 0) {
				//beatTime = (cntFixedUpdate-80)/10;
				if (cntFixedUpdate % 20 == 0) {
					//audio.PlayOneShot(audioClipBaseDrum, 0.75f);
					cameraScript.BeginShowMetronomLine();
				}

				BattleCameraScript.SetDebugTimeString("BeatTime["+beatTime+"] : " + cntFixedUpdate);

				//judge beat input

			}
		} else {
			//noss
			if (cntFixedUpdate % 10 == 0) {
				if (cntFixedUpdate % 20 == 0) {
					/*if(!isPlaySong)*/ audio.PlayOneShot(audioClipBaseDrum, 0.75f);
					cameraScript.BeginShowMetronomLine();
				}
				
				BattleCameraScript.SetDebugTimeString("SongTime["+(cntFixedUpdate/10)+"] : " + cntFixedUpdate);
			}
		}

		//cntFixedUpdateBeat++;
		cntFixedUpdate++;
		if (cntFixedUpdate >= 160) {
			//next songs
			cntFixedUpdate = 0;
			beatTime = -1;

		}
	}

	private void HitDrum(int drumIndex) {
		if(bAllowInputBeat) {
			BattleCameraScript.SetDebugInputString("Judge["+beatTime+"] : [" + judgeState + "]" +
					Beats.GetJudgeString(judgeState) + "(" + (cntFixedUpdate-1) + "," + judgeBeatCnt + ")");

			beats[beatTime].Set(judgeState, drumIndex);

			GameObject effectObj;
			Vector3 effectPos = new Vector3 (0, Random.Range(screenHeight*0.25f, screenHeight*0.75f), 10);

			switch (drumIndex) {
			case Beats.BEAT_KICK:
				effectPos.x = Random.Range(screenWidth*0.05f, screenWidth*0.15f);

				audio.PlayOneShot(audioClipKickDrum);
				effectObj = Instantiate(effectKickDrum) as GameObject;
				effectObj.transform.parent = gameCamera.transform;
				effectObj.transform.position = gameCamera.ScreenToWorldPoint(effectPos);
				break;

			case Beats.BEAT_SNARE:
				effectPos.x = Random.Range(screenWidth*0.85f, screenWidth*0.95f);

				audio.PlayOneShot(audioClipSnareDrum);
				effectObj = Instantiate(effectSnareDrum) as GameObject;
				effectObj.transform.parent = gameCamera.transform;
				effectObj.transform.position = gameCamera.ScreenToWorldPoint(effectPos);
				break;
			}
		}
	}

	private void PlaySong(int songType) {
		switch (songType) {
		case Beats.SONG_TYPE_FORWARD:
			audio.PlayOneShot(audioClipSongForward, 0.8f);
			break;

		case Beats.SONG_TYPE_ATTACK:
			audio.PlayOneShot(audioClipSongAttack, 0.8f);
			break;
		}
	}

	private void SoldierAction(int songType) {
		foreach (Soldier soldier in soldiers) {
			soldier.StartAction (songType);
		}
	}
}
