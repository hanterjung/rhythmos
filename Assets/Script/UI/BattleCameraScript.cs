﻿using UnityEngine;
using System.Collections;

public class BattleCameraScript : MonoBehaviour {
	public GUIStyle guiMetronomeLineStyle;

	private readonly float screenWidth, screenHeight;

	private Color savedColor;

	private Vector2 metronomeBoxPos = new Vector2 (4, 4);
	private Color metronomeColor;
	private float metronomeTimeCount;
	private float startTime;

	public Transform guiStringGroup;
	static public string strDebugString;
	static public GUIText guiTextDebugString;
	static public GUIText guiTimeDebugString, guiInputDebugString;

	BattleCameraScript() {
		screenWidth = Screen.width - 8;
		screenHeight = Screen.height - 8;
	}

	void Awake() {

		guiTextDebugString = guiStringGroup.FindChild ("GUIDebugString").GetComponent<GUIText> () as GUIText;
		guiTimeDebugString = guiStringGroup.FindChild ("GUIDebugTimeString").GetComponent<GUIText> () as GUIText;
		guiInputDebugString = guiStringGroup.FindChild ("GUIDebugInputString").GetComponent<GUIText> () as GUIText;

		//guiMetronomeLineStyle = new GUIStyle ();
		//guiMetronomeLineStyle.border = new RectOffset (1, 1, 1, 1);

		metronomeColor = new Color (1f, 1f, 1f, 1f);
		metronomeColor.a = 0f;
		metronomeTimeCount = 0f;
	}

	// Use this for initialization
	void Start () {
		//BeginShowMetronomLine ();
	} 
	
	// Update is called once per frame
	void Update () {
	}

	void OnGUI() {
		if(metronomeColor.a > 0f) {
			savedColor = GUI.color;

			metronomeColor.a = 1f * Mathf.Lerp(1f, 0f, metronomeTimeCount);
			metronomeTimeCount += Time.deltaTime;
			GUI.color = metronomeColor;
			GUI.Box (new Rect (metronomeBoxPos.x, metronomeBoxPos.y, screenWidth, screenHeight), GUIContent.none, guiMetronomeLineStyle);

			GUI.color = savedColor;
		}

	}

	public void BeginShowMetronomLine() {
		metronomeColor.a = 255f;
		metronomeTimeCount = 0f;
	}

	static public void SetDebugString(string debugStr) {
		strDebugString = debugStr;
		guiTextDebugString.text = strDebugString;
	}

	static public void SetDebugTimeString(string debugTimeStr) {
		guiTimeDebugString.text = debugTimeStr;
	}

	static public void SetDebugInputString(string debugInputStr) {
		guiInputDebugString.text = debugInputStr;
	}
}
