﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	void Awake () {
		//set resoultion
		float ratio = (float)(Screen.height) / Screen.width;
		Screen.SetResolution (800, (int)(800 * ratio), true);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			Application.LoadLevel("GameScene");
		}
	}

	void OnGUI() {
		GUI.Box(new Rect(Screen.width/2-100, Screen.height/2 - 80, 200, 160),
		        "GameStart\n(" + Screen.width+","+Screen.height);
	}
}
