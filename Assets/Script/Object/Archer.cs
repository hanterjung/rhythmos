﻿using UnityEngine;
using System.Collections;

public class Archer : Soldier {
	public float rangeMin = 8f;
	public float rangeMax = 20f;

	public GameObject projectile;

	public Archer() : base() {
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		base.Update ();
	}

	/*protected override void Forward() {
		base.Forward ();
	}*/

	protected override void Attack() {
		base.Attack ();

		Fire ();
	}

	protected virtual void Fire() {
		//enemy range;
		float dist = Mathf.Abs (GetNearestEnemyXPos () - transform.position.x);
		if(dist>rangeMax) dist = rangeMax;
		else if(dist<rangeMin) dist = rangeMin;

		// v*cosΘ * t = dist
		// v*sinΘ = 4.9 * t
		// v*cosΘ * v*sinΘ / 4.9 = dist
		// v*v * cosΘ*sinΘ / 4.9 = dist
		// v*v = dist * 4.9 / (cosΘ*sinΘ)
		// v = √(dist * 4.9 / (cosΘ*sinΘ))
		// t = v*sinΘ/4.9
		float velocity, degree, angle, time;

		//if Θ = 45°; //40~50°
		degree = Random.Range(40f, 50f);
		angle = degree * Mathf.Deg2Rad;
		velocity = Mathf.Sqrt (dist * 4.9f / (Mathf.Cos (angle) * Mathf.Sin (angle)));
		time = velocity * Mathf.Sin (angle) / 4.9f;
		
		GameObject projectile = Instantiate(this.projectile, transform.position, Quaternion.Euler(0,0,degree)) as GameObject;
		//projectile.rigidbody2D.AddForce (new Vector2 (300f, 300f), ForceMode2D.Force);
		projectile.rigidbody2D.AddForce (new Vector2 (Mathf.Cos(angle)*velocity,
		                                              Mathf.Sin(angle)*velocity), ForceMode2D.Impulse);
		//projectile.rigidbody2D.velocity = new Vector2 (10f, 10f);
		projectile.rigidbody2D.AddTorque (-degree * 2 * Mathf.Deg2Rad / time, ForceMode2D.Impulse);

	}

	float GetNearestEnemyXPos() {
		Soldier[] enemies = BattleGameController.GameController.enemySoldiers;
		float xpos = float.MaxValue;
		foreach (Soldier enemy in enemies) {
			if(xpos > enemy.transform.position.x) {
				xpos = enemy.transform.position.x;
			}
		}
		return xpos;
	}
}
