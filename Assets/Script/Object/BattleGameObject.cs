﻿using UnityEngine;
using System.Collections;

public class BattleGameObject : MonoBehaviour {

	public const int TEAM_NEUTRAL = 0;
	public const int TEAM_ENEMY = 1;
	public const int TEAM_BOSS = 2;
	public const int TEAM_USER = 10;	//use singleplayer
	public const int TEAM_LEFT = 10;	//use multiplayer
	public const int TEAM_RIGHT = 11;	//use multiplayer
	public int whatTeam = TEAM_NEUTRAL;

	public const int OBJ_TYPE_NATURE = 0;
	public const int OBJ_TYPE_BUILDING = 10;
	public const int OBJ_TYPE_PROJECTILE = 20;
	public const int OBJ_TYPE_SOLDIER = 30;
	public const int OBJ_TYPE_DRUMMER = 31;
	public const int OBJ_TYPE_HERO = 32;
	public int gameObjType = OBJ_TYPE_NATURE;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public Vector2 GetPosition() {
		return transform.position;
	}

	public float GetXPos() {
		return transform.position.x;
	}

	public float GetYPos() {
		return transform.position.y;
	}

	public void Init() {
	}
}
