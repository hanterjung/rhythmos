﻿using UnityEngine;
using System.Collections;

public class Beats : Object {
	//to judge input drum
	public const int JUDGE_PERFECT = 0;
	public const int JUDGE_EXCELLENT = 1;
	public const int JUDGE_GOOD = 2;
	public const int JUDGE_BAD = 3;
	public int judge = JUDGE_BAD;
	private static readonly string[] JUDGE_STRINGS = {
		"PERFECT", "EXCELLENT", "GOOD", "BAD"
	};
	
	//to check input drum beat order
	public const int BEAT_NONE = -1;
	public const int BEAT_KICK = 0;
	public const int BEAT_SNARE = 1;
	public int beatType = BEAT_NONE;

	public Beats(){
		Init ();
	}

	public Beats(int judge, int beatType) {
		this.judge = judge;
		this.beatType = beatType;
	}

	public Beats(int beatType) {
		this.beatType = beatType;
	}

	public void Init() {
		this.judge = JUDGE_BAD;
		this.beatType = BEAT_NONE;
	}

	public void Set(int judge, int beatType) {
		if(this.beatType == BEAT_NONE) {
			this.judge = judge;
			this.beatType = beatType;
		} else {
			this.judge = JUDGE_BAD;
		}
	}



	/*** beats compare ***/
	public const int SONG_TYPE_NONE = 0;
	public const int SONG_TYPE_FORWARD = 1;
	public const int SONG_TYPE_ATTACK = 2;
	public const int SONG_TYPE_DEFEND = 3;
	private static readonly string[] SONG_TYPE_STRINGS = {
		"SONG_TYPE_NONE", "SONG_TYPE_FORWARD", "SONG_TYPE_ATTACK", "SONG_TYPE_DEFEND"
	};

	private static readonly Beats[] BEATS_SONG_FORWARD = {
		new Beats(BEAT_KICK), new Beats(BEAT_NONE), new Beats(BEAT_KICK), new Beats(BEAT_NONE), 
		new Beats(BEAT_KICK), new Beats(BEAT_NONE), new Beats(BEAT_KICK), new Beats(BEAT_NONE)
	};

	private static readonly Beats[] BEATS_SONG_ATTACK = {
		new Beats(BEAT_KICK), new Beats(BEAT_NONE), new Beats(BEAT_SNARE), new Beats(BEAT_NONE), 
		new Beats(BEAT_SNARE), new Beats(BEAT_NONE), new Beats(BEAT_SNARE), new Beats(BEAT_NONE)
	};

	public static int GetSongType(Beats[] beats) {
		if(Compare(beats, BEATS_SONG_FORWARD)) return SONG_TYPE_FORWARD;
		else if(Compare(beats, BEATS_SONG_ATTACK)) return SONG_TYPE_ATTACK;

		return SONG_TYPE_NONE;
	}

	public static void Init(Beats[] beats) {
		for (int i=0; i<beats.Length; i++) {
			beats[i].Init();
		}
	}

	public static bool Compare(Beats[] beats, Beats[] checkBeats) {
		bool bEqaul = true;
		for(int i=0; i<checkBeats.Length; i++) {
			if(beats[i].beatType != checkBeats[i].beatType) {
				bEqaul = false;
				break;
			} else {
				if(beats[i].beatType != BEAT_NONE && beats[i].judge == JUDGE_BAD) {
					bEqaul = false;
					break;
				}
			}
		}
		return bEqaul;
	}

	public static string GetJudgeString(int judgeType) {
		return JUDGE_STRINGS[judgeType];
	}

	public static string GetSongTypeString(int songType) {
		return SONG_TYPE_STRINGS[songType];
	}
}
