﻿using UnityEngine;
using System.Collections;

public class Soldier : PlayerGameObject {
	protected const int ACTION_STATE_IDLE = Beats.SONG_TYPE_NONE;
	protected const int ACTION_STATE_FORWARD = Beats.SONG_TYPE_FORWARD;
	protected const int ACTION_STATE_ATTACK = Beats.SONG_TYPE_ATTACK;
	protected const int ACTION_STATE_DEFEND = Beats.SONG_TYPE_DEFEND;

	protected int actionState = ACTION_STATE_IDLE;
	protected long startActionTime = 0L;
	protected float actionPerformingTime = 0.0f;

	protected Vector3 actionStartPos = new Vector3();

	public float moveDist = 4.0f;
	public float attackSpeed = 1.0f;
	public float range = 2f;

	protected Animator animator;

	
	public Soldier() {
		gameObjType = OBJ_TYPE_SOLDIER;
	}

	protected void Awake() {
		animator = GetComponent<Animator> ();
	}

	// Use this for initialization
	protected void Start () {
		animator.Play ("Idle");
	}
	
	// Update is called once per frame
	protected void Update () {

		actionPerformingTime += Time.deltaTime;
		switch (actionState) {
		case ACTION_STATE_FORWARD :
			/*transform.position = new Vector3(Mathf.Lerp(startPos.x, startPos.x + 5.0f, actionPerformingTime / 2.0f),
											 startPos.y, startPos.z);*/
			/*transform.position = new Vector3(startPos.x + 5 * Mathf.Sin(Mathf.PI/2 * actionPerformingTime / 2.0f),
											 startPos.y, startPos.z);*/
			transform.position = new Vector3(actionStartPos.x + moveDist * Spline.Ease(actionPerformingTime/2.0f, EasingType.Sine),
			                                 actionStartPos.y, actionStartPos.z);
			break;
		}
		if (actionPerformingTime >= 2.0f) actionState = ACTION_STATE_IDLE;
	}

	public void StartAction(int songType) {
		actionPerformingTime = 0.0f;

		switch (songType) {
		case Beats.SONG_TYPE_FORWARD:
			Forward();
			break;
		case Beats.SONG_TYPE_ATTACK:
			Attack();
			break;
		}
	}

	protected virtual void Forward() {
		Debug.Log ("forward!!!");
		//rigidbody2D.AddForce (new Vector2 (180f, 0));
		actionState = ACTION_STATE_FORWARD;
		actionStartPos = transform.position;
		animator.Play ("Idle");
	}

	protected virtual void Attack() {
		actionState = ACTION_STATE_ATTACK;
		actionStartPos = transform.position;
		animator.Play ("Attack");
	}

	public void OnAttackActionEnded() {
		
	}
}
