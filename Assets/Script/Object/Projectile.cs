﻿using UnityEngine;
using System.Collections;

abstract public class Projectile : BattleGameObject {

	public GameObject explosionEffect;
	public float explosionSizeMax = 1.1f;
	public float explosionSizeMin = 0.9f;

	public Projectile() {
		gameObjType = OBJ_TYPE_PROJECTILE;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnExplode()
	{
		// Create a quaternion with a random rotation in the z-axis.
		Quaternion randomRotation = Quaternion.Euler(0f, 0f, Random.Range(0f, 360f));
		
		if (explosionEffect == null) return;
		
		// Instantiate the explosion where the rocket is with the random rotation.
		GameObject newExplosion = Instantiate(explosionEffect, transform.position, randomRotation) as GameObject;
		float size = Random.Range (explosionSizeMin, explosionSizeMax);
		newExplosion.transform.localScale = new Vector3 (size, size, 1);
	}

	void OnTriggerEnter2D (Collider2D col) 
	{
		// If it hits an enemy...
		if(col.tag == "Soldier")
		{
			// ... find the Enemy script and call the Hurt function.
			//col.gameObject.GetComponent<Soldier>().Hurt();
			
			// Call the explosion instantiation.
			OnExplode();
			
			// Destroy the projectile.
			Destroy (gameObject);
		}
		// Otherwise if it hits to ground
		else if(col.tag == "Ground")
		{		
			// Destroy the rocket.
			Destroy (gameObject);
		}
	}
}
